<?php
  //include 'dbConnect.php';
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>AJAX Group Project</title>
<style type="text/css">

body  {
  text-align: center;
  background-color: orange;
  border-style: solid;
}

.bold {
    font-weight: bold;
}

#ourDescription	{
	color: blue;
}

img   {
  border-style: solid;
  border-color: black;
}

</style>
    <script src="jquery-3.1.0.js"></script>
    <script type="text/javascript">
       function GetSelectedValue(ourSelect) {
       var selectedValue = ourSelect.value;

       if (selectedValue == "van") {
         $("#ourDescription").load("van.php");
       }

       if (selectedValue == "choc") {
         $("#ourDescription").load("choc.php");
       }

		if (selectedValue == "red") {
         $("#ourDescription").load("red.php");
       }  
   }
</script>

</head>

<body>
<h3>WDV321 Advanced Javascript</h3>
<h4>AJAX Team Project</h4>
<h5>Team Members: Jeremy, Jayden, Josh </h5>
<p>&nbsp;</p>
<form name="form1" method="post" action="">
<img src="cupcake.jpg">
 <h1>TRICK OR TREAT!!!</h1>
 <p>Make your choice here: 
   <label>
     <select name="ourSelect" id="select" onchange="GetSelectedValue(this)">
       <option value="van">Vanilla Cupcake</option>
       <option value="choc">Chocolate Cupcake</option>
       <option value="red">Red Velvet Cupcake</option>
     </select>
   </label>
 </p>
 <p>Description:</p>
 <p id="ourDescription"></p>
 <!-- <p>
   <input type="submit" name="button" id="button" value="Submit">
   <input type="submit" name="button2" id="button2" value="Submit">
 </p> -->
</form>
</body>
</html>