<?php
echo "<div class='row'>
            <div class='col-lg-12'>
                <h1 class='page-header'>TACO CHILI
                    <small> Tex-Mex Wonderfulness!</small>
                </h1>
            </div>
        </div>
        <div class='row'>

            <div class='col-md-8'>
                <img class='img-responsive' src='images/tacochili.jpg' alt='taco chili picture'>
            </div>

            <div class='col-md-4 jerbold'>
                <h3>BEEFY TACO GOODNESS</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                <h3>WHAT YOU NEED TO KNOW</h3>
                <ul>
                    <li>Serves: 8</li>
                    <li>Prep Time: 15 min</li>
                    <li>Cooking Time: 4-8 hrs</li>
                </ul>
            </div>

        </div>
        <!-- /.row -->

        <div class='row'>

            <div class='col-md-6 batchDiv'>
                <h3>CHOOSE YOUR BATCH SIZE:</h3>
            </div>

            <div class='col-md-6 batchDiv'>
                <input type='radio' name='batch' value='regular' id='regular' class='hvr-grow' onclick='addIngredPrepRowsNormal()'> Regular 8 serving batch<br><br>
                <input type='radio' name='batch' value='half' id='half' class='hvr-grow' onclick='addIngredPrepRowsHalf()'> Half sized 4 serving batch<br><br>
                <input type='radio' name='batch' value='double' id='double' class='hvr-grow' onclick='addIngredPrepRowsDouble()'> Double sized: 16 serving batch<br><br>
            </div>

        </div>
        <!-- /.row -->

        <!-- Ingredients List Row -->
        <div class='row animated fadeIn' id='ingredientsRowNormal'>

            <div class='col-lg-12'>
                <h3 class='page-header'>NORMAL BATCH INGREDIENTS LIST</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>1 1/2 lb ground beef</li>
                        <li>2 (10-oz) cans diced tomatoes with chiles, undrained</li>
                        <li>1 can chili beans in sauce (15–16 oz), undrained </li>
                        <li>1 can red kidney beans (15–16 oz), undrained</li>
                        <li>1 (15.25-oz) can Southwestern corn with peppers, drained</li>
                        <li>1 (15.5-oz) can hominy, drained</li>
                        <li>1 (10-oz) can mild enchilada sauce</li>
                        <li>1 packet taco seasoning mix (about 1 oz)</li>
                    </ul>
                </p>
                <input type='button' id='hideshow' value='Show / Hide Preparation Instructions'>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/veggies4.jpg' alt='fresh ingredients picture'>
            </div>

        </div>
        <!-- /.row -->

        <!-- Ingredients List Row -->
        <div class='row animated fadeIn' id='ingredientsRowHalf'>

            <div class='col-lg-12'>
                <h3 class='page-header'>HALF BATCH INGREDIENTS LIST</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>3/4 lb ground beef</li>
                        <li>1 (10-oz) cans diced tomatoes with chiles, undrained</li>
                        <li>1/2 can chili beans in sauce (15–16 oz), undrained </li>
                        <li>1/2 can red kidney beans (15–16 oz), undrained</li>
                        <li>1/2 (15.25-oz) can Southwestern corn with peppers, drained</li>
                        <li>1/2 (15.5-oz) can hominy, drained</li>
                        <li>1/2 (10-oz) can mild enchilada sauce</li>
                        <li>1/2 packet taco seasoning mix (about 1 oz)</li>
                    </ul>
                </p>
                <input type='button' id='hideshow2' value='Show/Hide Preparation Instructions'>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/veggies5.jpg' alt='fresh ingredients picture'>
            </div>

        </div>
        <!-- /.row -->

        <!-- Ingredients List Row -->
        <div class='row animated fadeIn' id='ingredientsRowDouble'>

            <div class='col-lg-12'>
                <h3 class='page-header'>DOUBLE BATCH INGREDIENTS LIST</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>3 lb ground beef</li>
                        <li>4 (10-oz) cans diced tomatoes with chiles, undrained</li>
                        <li>2 can chili beans in sauce (15–16 oz), undrained </li>
                        <li>2 can red kidney beans (15–16 oz), undrained</li>
                        <li>2 (15.25-oz) can Southwestern corn with peppers, drained</li>
                        <li>2 (15.5-oz) can hominy, drained</li>
                        <li>2 (10-oz) can mild enchilada sauce</li>
                        <li>2 packet taco seasoning mix (about 1 oz)</li>
                    </ul>
                </p>
                <input type='button' id='hideshow3' value='Show/Hide Preparation Instructions'>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/veggies6.jpg' alt='fresh ingredients picture'>
            </div>

        </div>
        <!-- /.row -->

        <!-- Preparations Row -->
        <div class='row animated fadeIn' id='preparationRow'>

            <div class='col-lg-12'>
                <h3 class='page-header'>PREPARATION INSTRUCTIONS</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>Preheat large, nonstick sauté pan on medium-high 2–3 minutes.</li>
                        <li>Place beef in pan (wash hands).</li>
                        <li>Brown 5–7 minutes, stirring to crumble meat, or until no pink remains.</li>
                        <li>Stir in taco seasoning.</li>
                        <li>Remove meat from pan.</li>
                        <li>Combine remaining ingredients in slow cooker; stir in meat.</li>
                        <li>Cover and cook on HIGH 3–4 hours or LOW 6–8 hours.</li>
                        <li>Serve.</li>
                    </ul>
                </p>
            </div>
            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/tacochili2.jpg' alt='chili bowl picture'>
            </div>
        </div>"
    ?>