<?php
echo "<div class='fallingLeaves'>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>

        <div class='row'>
            <div class='col-lg-12'>
                <h1 class='page-header'>CROCKPOT CHILI
                    <small> A fall favorite!</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class='row'>

            <div class='col-md-8'>
                <img class='img-responsive' src='images/chili.png' alt='crock pot chili picture'>
            </div>

            <div class='col-md-4 jerbold'>
                <h3>BEEF OR CHICKEN, YOU MAKE THE CALL.</h3>
                <p>The ultimate fall comfort food, chili is simple and inviting fill-you-up fare. Whether you want to serve a bowl at your next tailgate, as a simple weeknight supper or for lunch on a lazy Saturday, check out our crockpot chili recipe. </p>
                <h3>WHAT YOU NEED TO KNOW</h3>
                <ul>
                    <li>Serves: 6</li>
                    <li>Prep Time: 25 min</li>
                    <li>Cooking Time: 6 hrs</li>
                </ul>
            </div>

        </div>
        <!-- /.row -->

        <div class='row'>

            <div class='col-md-6 batchDiv'>
                <h3>CHOOSE YOUR BATCH SIZE:</h3>
            </div>

            <div class='col-md-6 batchDiv'>
                <input type='radio' name='batch' value='regular' id='regular' class='hvr-grow' onclick='addIngredPrepRowsNormal()'> Regular 6 serving batch<br><br>
                <input type='radio' name='batch' value='half' id='half' class='hvr-grow' onclick='addIngredPrepRowsHalf()'> Half sized 3 serving batch<br><br>
                <input type='radio' name='batch' value='double' id='double' class='hvr-grow' onclick='addIngredPrepRowsDouble()'> Double sized: 12 serving batch<br><br>
            </div>

        </div>
        <!-- /.row -->

        <!-- Ingredients List Row -->
        <div class='row animated fadeIn' id='ingredientsRowNormal'>

            <div class='col-lg-12'>
                <h3 class='page-header'>NORMAL BATCH INGREDIENTS LIST</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>2 tbsp. cooking oil
                            <li>1 cup onion</li>
                            <li>1 cup chopped peppers</li>
                            <li>4 tbsp. Chili powder</li>
                            <li>1 tsp. Hot chili powder (optional)</li>
                            <li>1 lb ground beef or chicken</li>
                            <li>2 cans Red Beans</li>
                            <li>2 cans Kidney Beans</li>
                            <li>2 cans Tomato Puree</li>
                            <li>2 cans Tomato Sauce</li>
                            <li>1 cup shredded cheese (optional)</li>
                            <li>1/2 cup sour cream (optional)</li>
                    </ul>
            <div class='row animated fadeIn' id='preparationRow'>

            <div class='col-lg-12'>
                <h3 class='page-header'>PREPARATION INSTRUCTIONS</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>Heat cooking oil in 2 quart skillet.</li>
                        <li>Saute the onions and peppers for 5 minutes.</li>
                        <li>Add spices and stir for 30 seconds.</li>
                        <li>Add meat and cook until browned. Approximately 15 minutes.</li>
                        <li>Pour contents of skillet into crock pot.</li>
                        <li>Rinse the beans and place in crockpot.</li>
                        <li>Open and Pour the tomato puree and sauce into crock pot.</li>
                        <li>Cover crockpot and cook on low for 6 hours.</li>
                        <li>Serve into individual bowls and top with sour cream and cheese.</li>
                    </ul>
                </p>
            </div>
            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/chili2.jpg' alt='chili bowl picture'>
            </div>
        </div> 
            </div>

            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/veggies.jpg' alt='fresh ingredients picture'>
            </div>

        </div>
        <!-- /.row -->

        <!-- Ingredients List Row -->
        <div class='row animated fadeIn' id='ingredientsRowHalf'>

            <div class='col-lg-12'>
                <h3 class='page-header'>HALF BATCH INGREDIENTS LIST</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>1 tbsp. cooking oil
                            <li>1/2 cup onion</li>
                            <li>1/2 cup chopped peppers</li>
                            <li>2 tbsp. Chili powder</li>
                            <li>1/2 tsp. Hot chili powder (optional)</li>
                            <li>1/2 lb ground beef or chicken</li>
                            <li>1 can Red Beans</li>
                            <li>1 can Kidney Beans</li>
                            <li>1 can Tomato Puree</li>
                            <li>1 can Tomato Sauce</li>
                            <li>1/2 cup shredded cheese (optional)</li>
                            <li>1/4 cup sour cream (optional)</li>
                    </ul>
                </p>
                <input type='button' id='hideshow2' value='Show/Hide Preparation Instructions'>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/veggies2.png' alt='fresh ingredients picture'>
            </div>

        </div>
        <!-- /.row -->

        <!-- Ingredients List Row -->
        <div class='row animated fadeIn' id='ingredientsRowDouble'>

            <div class='col-lg-12'>
                <h3 class='page-header'>DOUBLE BATCH INGREDIENTS LIST</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>4 tbsp. cooking oil
                            <li>2 cups onion</li>
                            <li>2 cups chopped peppers</li>
                            <li>8 tbsp. Chili powder</li>
                            <li>2 tsp. Hot chili powder (optional)</li>
                            <li>2 lbs ground beef or chicken</li>
                            <li>4 cans Red Beans</li>
                            <li>4 cans Kidney Beans</li>
                            <li>4 cans Tomato Puree</li>
                            <li>4 cans Tomato Sauce</li>
                            <li>2 cups shredded cheese (optional)</li>
                            <li>1 cup sour cream (optional)</li>
                    </ul>
                </p>
                <input type='button' id='hideshow3' value='Show/Hide Preparation Instructions'>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/veggies3.png' alt='fresh ingredients picture'>
            </div>

        </div>
        <!-- /.row -->

        <!-- Preparations Row -->
        <div class='row animated fadeIn' id='preparationRow'>

            <div class='col-lg-12'>
                <h3 class='page-header'>PREPARATION INSTRUCTIONS</h3>
            </div>

            <div class='col-sm-6 col-xs-12'>
                <p>
                    <ul>
                        <li>Heat cooking oil in 2 quart skillet.</li>
                        <li>Saute the onions and peppers for 5 minutes.</li>
                        <li>Add spices and stir for 30 seconds.</li>
                        <li>Add meat and cook until browned. Approximately 15 minutes.</li>
                        <li>Pour contents of skillet into crock pot.</li>
                        <li>Rinse the beans and place in crockpot.</li>
                        <li>Open and Pour the tomato puree and sauce into crock pot.</li>
                        <li>Cover crockpot and cook on low for 6 hours.</li>
                        <li>Serve into individual bowls and top with sour cream and cheese.</li>
                    </ul>
                </p>
            </div>
            <div class='col-sm-6 col-xs-12'>
                <img class='img-responsive portfolio-item' src='images/chili2.jpg' alt='chili bowl picture'>
            </div>
        </div>"
    ?>