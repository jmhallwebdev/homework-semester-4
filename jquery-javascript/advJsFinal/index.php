<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dynamic Recipe Page</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
    <link href="css/hover-min.css" rel="stylesheet">
    <link href="css/leafstyle.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand navTitleJer animated fadeInLeft" href="#">Nuthin But Chili!</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="container imgBorder" id="bgimg">

        <div class="jerInner">
            <h1>NUTHIN  BUT CHILI</h1>
            <img src="images/indexchili2.jpg" alt="chili background picture">
            <p></p>
            <br>
            <p> 
                <label>
                    <select name="ourSelect" id="select" onchange="GetSelectedValue(this)">
                        <option>Select Your Recipe Here!</option>
                        <option value="crockpotchili">Crock Pot Chili</option>
                        <option value="tacochili">Taco Chili</option>
                        <option value="pheasantchili">Pheasant Chili</option>
                    </select>
                </label>
            </p>
        </div>

        <div id="ourDescription">

        </div>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Recipes For Days 2016</p>
                </div>
            </div>
        </footer>

    </div>

    <script src="js/jquery-3.1.0.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.1.0.js"></script>

    <script type='text/javascript' data-cfasync='false' src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js' data-shr-siteid='5ae7042b0957afec7c0e7d0d70cfaa7d' async='async'></script>

    <script type="text/javascript">
           function GetSelectedValue(ourSelect) {
           var selectedValue = ourSelect.value;

           if (selectedValue == "crockpotchili") {
             $("#ourDescription").load("crockpotchili.php");
           }

           if (selectedValue == "tacochili") {
             $("#ourDescription").load("tacochili.php");
           }

            if (selectedValue == "pheasantchili") {
             $("#ourDescription").load("pheasantchili.php");
           }  
       }

       function addIngredPrepRowsNormal() {
            $(ingredientsRowHalf).css("display", "none");
            $(ingredientsRowDouble).css("display", "none");
            $(ingredientsRowNormal).css("display", "inherit");
            }

        function    addIngredPrepRowsHalf() {
            $(ingredientsRowNormal).css("display", "none");
            $(ingredientsRowDouble).css("display", "none");
            $(ingredientsRowHalf).css("display", "inherit");
            }

        function    addIngredPrepRowsDouble() {
            $(ingredientsRowNormal).css("display", "none");
            $(ingredientsRowHalf).css("display", "none");
            $(ingredientsRowDouble).css("display", "inherit");
            }

        function    closeAllPrep() {
            $(preparationRow).css("display", "none");
            }

            jQuery(document).ready(function(){
            jQuery('#hideshow').on('click', function(event) {        
            jQuery('#preparationRow').toggle('show');
        });
    });
            jQuery(document).ready(function(){
            jQuery('#hideshow2').on('click', function(event) {        
            jQuery('#preparationRow').toggle('show');
        });
    });
            jQuery(document).ready(function(){
            jQuery('#hideshow3').on('click', function(event) {        
            jQuery('#preparationRow').toggle('show');
        });
    });
    </script>

</body>
</html>