// 1. Create a function that uses a regular expression to validate an email address field.

		// based upon: RegExpObject.test(string)

	function isValidEmailAddress(emailAddress) 
		{
    		var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

    		var myResult = pattern.test(emailAddress);

			if (myResult === false)
				{ 
					alert("Sorry, the entered information does not meet the required format."); 
				}
		}

// 2. Create a function that will validate a phone number field.  You may determine the expected format.

		// based upon string.match(regexp)

	function phoneNumberValidate(inputTxt) 
		{
  			var phoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  			
  			if(inputTxt.value.match(phoneNo)) 
  				{
    				return true;
  				}
  			
  			else 
  				{
    				alert("Sorry, the entered information does not meet the required format.");
    				return false;
  				}	
		}

// 3. Create a function that will validate a zip code field.  You may determine the expected format. 


		// based upon string.match(regexp)

	function zipCodeValidate(inputTxt)
		{
			var isValidZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/;

			if(inputTxt.value.match(zipCodeValidate)) 
  				{
    				return true;
  				}
  			
  			else 
  				{
    				alert("Sorry, the entered information does not meet the required format.");
    				return false;
  				}	
		}

// 4. Create a function that will replace the following special characters in a string  ' / < > with "-".


		// based upon string.replace(searchvalue,newvalue)

	function replaceSpecialChars(inputTxt)
		{
			var str = inputTxt;
			var res = str.replace(/['/<>]/g, "-"); 
		}

		// we now have res which contains the modified string 