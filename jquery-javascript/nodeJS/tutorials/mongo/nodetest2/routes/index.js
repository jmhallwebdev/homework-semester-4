var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'An example of: NodeJS, Express, Jade, & MongoDB' });
});

module.exports = router;
