var express = require('express');
var router = express.Router();

router.get('/anotherpage', function(req, res, next) {
  res.render('anotherpage', { title: 'something different!' });
});

module.exports = router;
