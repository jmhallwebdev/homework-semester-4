            <?php include("email.php"); ?>

            <?php

            $nameErrMsg = "";
            $emailErrMsg = "";   
            $respErrMsg = "";
            $commentErrMsg = "";    

            $validForm = false;

            $inName = "";
            $inEmail = "";    
            $inResp = "";
            $inComment = "";    

            function validateName() {
              global $inName, $validForm, $nameErrMsg;    

              $nameErrMsg = "";                
              $inName = trim($inName);

                      if($inName=="")     
                      {
                        $validForm = false;         
                        $nameErrMsg = "Name Is Required"; 
                      }

                      if (!preg_match("/^[a-zA-Z ]*$/",$inName)) {
                        $validForm = false;
                        $nameErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

            function validateEmail()     
            {
              global $inEmail, $validForm, $emailErrMsg;    

              $inEmail = trim($inEmail);

                      if($inEmail=="")     
                      {
                        $validForm = false;         
                        $emailErrMsg = "Email Address Is Required"; 
                      }
                      if (!filter_var($inEmail, FILTER_VALIDATE_EMAIL)) {
                        $emailErrMsg = "Invalid email format"; 
                      }
                    }

            function validateResp()   
            {
              global $inResp, $validForm, $respErrMsg;  

              $respErrMsg = "";

                      if ($inResp =="")
                        {
                        $validForm = false;
                        $respErrMsg = "Reason For Contact Is Required";
                        } 
                    }

            function validateComment()      
            {
              global $inComment, $validForm, $commentErrMsg, $inResp;    

                      if ($inResp == "other" && $inComment == "")
                      {
                        $validForm = false;
                        $commentErrMsg = "Comments Required When Reason For Contact Is 'Other'";
                      }
                      if (!preg_match("/^[a-zA-Z ]*$/",$inComment)) {
                        $validForm = false;
                        $commentErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

if  (isset($_POST['submit']))
        {
          $inName = $_POST['inName'];
          $inEmail = $_POST['inEmail'];
          $inResp = $_POST['inContactReason'];
          $inComment = $_POST['inComment'];

          $validForm = true;

          validateName();
          validateEmail();
          validateResp();
          validateComment();
        }?>

        <!DOCTYPE HTML>
        <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <title>PROGRAMMING PROJECT: Contact Form With PHP Validation</title>
          <style>
            .red  {
              color:red;
              font-style:italic;  
            }
          </style>
        </head>

        <body>
          <h1>WDV341 Intro PHP</h1>
          <h2>PROGRAMMING PROJECT: Contact Form With PHP Validation</h2>
          
            <?php

            if ($validForm)
              {
                             $newEmail = new myEmail($myEmail_to, $myEmail_subject, $myEmail_body, $myEmail_from);   

                             $newEmail->setFrom($inEmail); // ** this isnt working **
                             $newEmail->setTo("jmhall4@dmacc.edu");
                             $newEmail->setSubject("Customer Contact Form");
                             $newEmail->setBody("Reason for contact: " . " " . $inResp . " " . "\nCustomer added comments: " . " " . $inComment);

                   $newEmail->sendEmail();    

                  ?>

                  <h2>Thank you for contacting us, here are the details of your communication:</h2>
              
                  <?php 
                  echo "<h3>Your email address is: </h3>" . $newEmail->getFrom() . "<br>"; 
                  echo "<h3>Our email address is: </h3>" . $newEmail->getTo() . "<br>";
                  echo "<h3>Subject Line is: </h3>" . $newEmail->getSubject() . "<br>";
                  echo "<h3>Email Body is: </h3>" . $newEmail->getBody() . "<br>";
                  echo "<h3>Your email was sent at: </h3>" . date("l jS \of F Y h:i:s A");

                  ?>

                  <?php
              } //end the true branch of the form view area



else

      { 
        ?>

      <header>Contact Form</header>
      <form name="form1" class="topBefore" method="post" action="contactForm.php">
        <p>&nbsp;</p>
        <p>
          <label>Your Name:
            <input type="text" name="inName" id="inName" value="<?php echo $inName;?>"><p class="red"><?php echo "$nameErrMsg";?></p>
          </label>
        </p>
        <p>Your Email: 
          <input type="text" name="inEmail" id="inEmail" value="<?php echo $inEmail;?>"/><p class="red"><?php echo "$emailErrMsg";?></p>
        </p>
        <p>Reason for contact: <p class="red"><?php echo "$respErrMsg";?></p>
          <label>
            <select name="inContactReason" id="inContactReason">
              <option value="">Please Select a Reason</option>
              <option value="problem"<?php if($inResp == 'problem'){echo("selected");}?>>Product Problem</option>
              <option value="return"<?php if($inResp == 'return'){echo("selected");}?>>Return a Product</option>
              <option value="other"<?php if($inResp == 'other'){echo("selected");}?>>Other</option>
            </select>
          </label>
        </p>
        <p>
          <label>Comments:
            <textarea name="inComment" id="inComment" cols="45" rows="5"/><?php echo $inComment;?></textarea>
          </label>
        </p>
        <p class="red"><?php echo "$commentErrMsg";?></p>
        </p>
        <p>
          <input type="submit" name="submit" id="submit" value="Submit">
          <input type="reset" name="reset" id="reset" value="Reset">
        </p>
      </form>

      <?php
      } //end else branch for the View area
      ?>

</body>
</html>
