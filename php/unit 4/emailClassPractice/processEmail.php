<!-- THE CONTROLLER -->

	<!-- This is where my class is -->
<?php require("email.php"); ?>



	<!-- create a new myEmail object called "newEmail" and loading in constructor values-->
<?php


	 // NSTANTIATE THE NEW OBJECT  

	 $newEmail = new myEmail($myEmail_from, $myEmail_to, $myEmail_subject, $myEmail_body);		

	 // USE SETTERS TO LOAD THE INFO

	 $newEmail->setFrom("jer@jersrealm.com");
	 $newEmail->setTo("midwestmetalhead@gmail.com");
	 $newEmail->setSubject("Inquiring About The End Of The World");
	 $newEmail->setBody("Please send me more information STAT!");

	$newEmail->sendEmail();    //this activates the sendMail method of the myEmail class. 
?>

<!-- END CONTROLLER -->



<!-- THE VIEW -->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>WDV341 PROGRAMMING PROJECT: Email Class
</title>
</head>
<body>
	<h1>WDV341 PROGRAMMING PROJECT: Email Class</h1>
	<h2>Thank you for contacting us, here are the details of your communication:</h2>
		<?php 
			echo "Sender's address is: " . $newEmail->getFrom() . "<br>"; 
			echo "Send to address is: " . $newEmail->getTo() . "<br>";
			echo "Subject Line is: " . $newEmail->getSubject() . "<br>";
			echo "Mesage Body is: " . $newEmail->getBody() . "<br>"; 
		?>
</body>
</html>

<!-- END VIEW -->