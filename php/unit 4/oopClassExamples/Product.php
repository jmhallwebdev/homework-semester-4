<?php                        // WRITING A CLASS
	class Product
		{
			//created 9/20/2016 by Jeremy Hall WDV341 Fall 2016. w/Jeff Gullion

			private $productName;	//define a property (local scope)  NAME
			private $quantity;		//define a property (local scope)  QUANTITY

			// SETTERS START HERE

			function setProductName($inProductName) 
				{
				$productName = $inProductName; //simple assigment through the Set method
				}			
			function setQuantity($inQuantity) 
				{
				$quantity = $inQuantity; //simple assigment through the Set method
				}	

			// SETTERS START HERE

			function getProductName()
				{
					return $productName;	//sends	the value stored in this object/variable to program
				}			
			function getQuantity()
				{
					return $quantity;	//sends	the value stored in this object/variable to program
				}	

			// METHODS START HERE

			function decreaseQuantity($inReduceQuantity)
				{
					$quantity -= $inReduceQuantity;	//add new order to existing quantity
				}		
			function increaseQuantity($inNewQuantity)
				{
					$quantity += $inNewQuantity;	//subtract new order to existing quantity
				}
		}	//end Product class

?>