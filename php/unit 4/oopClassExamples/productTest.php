<?php
	require 'Product.php'; 	//include the class file

	$product1 = new Product();

	$product1 -> setProductName("Pencil");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>OOP CLASS EXAMPLES IN CLASS 9/20/2016</title>
</head>
<body>
	<h1>WDV341 Test Page For Product Class</h1>

	<h2>Product1 Name is <?php echo $product1->getProductName(); ?></h2>
</body>
</html>