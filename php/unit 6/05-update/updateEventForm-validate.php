<?php
session_start();
if ($_SESSION['validUser'] == "yes")	//If this is a valid user allow access to this page
{	
	include 'dbConnect.php';
	if(isset($_POST["submit"]))
	{	
		//The form has been submitted and needs to be processed
		
		//Get the name value pairs from the $_POST variable into PHP variables
		//The example uses variables with the same name as the name atribute from the form
		$event_name = $_POST['event_name'];
  		$event_description = $_POST['event_description'];
  		$event_presenter = $_POST['event_presenter'];
  		$event_date = $_POST['event_datepicker'];
  		$event_time = $_POST['event_timepicker'];
      	$event_id = $_POST['event_id'];	//from the hidden field of the update form
		
		//Create the SQL UPDATE query or command  
		$sql = "UPDATE wdv341_event SET " ;
        $sql .= "event_name=?, ";
        $sql .= "event_description=?, ";
        $sql .= "event_presenter=?, ";
        $sql .= "event_date=?, ";
        $sql .= "event_time=? ";    
        $sql .= " WHERE (event_id='$event_id')"; //VERY IMPORTANT  
	
		$query = $link->prepare($sql);	//Prepare SQL query
	
		$query->bind_param("sssss",$event_name,$event_description,$event_presenter,$event_date,$event_time);
	
		if ( $query->execute() )
		{
			$message = "<h1>Your record has been successfully UPDATED the database.</h1>";
			$message .= "<p>Please <a href='http://www.jeremymhall.info/files/mySELECT/selectEvents.php'>view</a> your records.</p>";
		}
		else
		{
			$message = "<h1>You have encountered a problem.</h1>";
			$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}
				
	}//end if submitted
	else	
	{
		//The form needs to display the fields of the record to the user for changes
		$updateRecId = $_GET['event_id'];	//Record Id to be updated
		//$updateRecId = 2;				//Hard code a key for testing purposes
		
		echo "<h1>updateRecId: $updateRecId</h1>";
		
		//Finds a specific record in the table
		$sql = "SELECT event_id,event_name,event_description,event_presenter,event_date,event_time FROM wdv341_event WHERE event_id=?";	
	
		$query = $link->prepare($sql);
		
		$query->bind_param("i",$updateRecId);	
	
		if( $query->execute() )	//Run Query and Make sure the Query ran correctly
		{
			$query->bind_result($event_id,$event_name,$event_description,$event_presenter,$event_date,$event_time);
		
			$query->store_result();
			
			$query->fetch();
		}
		else
		{
			$message = "<h1>You have encountered a problem with your update.</h1>";
			$message .= "<h2>" . mysqli_error($link) . "</h2>" ;			
		}
	
	}//end else submitted
}//end Valid User True
else
{
	//Invalid User attempting to access this page. Send person to Login Page
	header('Location: presentersLogin.php');
}	
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Event Admin Example</title>
    <style>
        .red  {
        color:red;
        font-style:italic;  
              }
    </style>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <link href="jquery-ui-timepicker-addon.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="jquery-ui-timepicker-addon.js"></script>
    <script>
      $(function() {
                $("#event_datepicker").datepicker({
                   dateFormat:"yy-mm-dd",
                });
             });
    </script>
    <script>
      $(function() {
                $('#event_timepicker').timepicker();
             });
    </script>
    <script type="text/javascript">
  function validateMyForm() {
    // The field is empty, submit the form.
    if(!document.getElementById("honeypot").value) { 
      return true;
    } 
     // the field has a value it's a spam bot
    else {
      return false;
    }
  }
</script>
</head>
<body>
    <h1>WDV341 Event Admin System Example</h1>
    <h3>Input Form for Adding Events</h3>

<?php
//If the user submitted the form the changes have been made
if(isset($_POST["submit"]))
{
	echo $message;	//contains a Success or Failure output content
}//end if submitted

else
{	//The page needs to display the form and associated data to the user for changes
?>
<header>Event Form</header>
      <form id="updateEventForm" name="updateEventForm" method="post" action="updateEventForm.php" onsubmit="return validateMyForm();">
      <p>Add a new Event</p>
      <p>Event Name: 
      <input type="text" name="event_name" id="event_name" value="<?php echo $event_name;?>"></p><p class="red"><?php echo "$nameErrMsg";?></p>
      <p>Event Description:  
      <input type="text" name="event_description" id="event_description" value="<?php echo $event_description;?>"></p><p class="red"><?php echo "$descErrMsg";?></p>
      <p>Event Presenter: 
      <input type="text" name="event_presenter" id="event_presenter" value="<?php echo $event_presenter;?>"></p><p class="red"><?php echo "$presErrMsg";?></p>
      <p>Date: <input type="text" value="<?php echo $event_date;?>" name = "event_datepicker" id="event_datepicker"></p>
      <p class="red"><?php echo "$dateErrMsg";?></p>
      <p>Time: <input type="text" value="<?php echo $event_time;?>" name = "event_timepicker" id="event_timepicker"></p>
      <p class="red"><?php echo "$timeErrMsg";?></p>
      <p><input type="hidden" name="event_id" id="event_id"
      value="<?php echo $event_id;?>"/></p>
      <!--Honeypot is below -->
      <div style="display:none;">
      <label>Keep this field blank</label>
      <input type="text" name="honeypot" id="honeypot" />
      </div>
      <!--Honeypot is above -->

      <p>
      <input type="submit" name="submit" id="submit" value="Add Event" />
      <input type="reset" name="button2" id="button2" value="Clear Form" />
      </p>
      </form>

<?php
}//end else submitted
$query->close();
$link->close();
?>
<p>Return to <a href="presentersLogin.php">Administrator Options</a></p>
</body>
</html>
