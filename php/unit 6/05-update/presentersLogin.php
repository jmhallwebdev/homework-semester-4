<?php	
	include 'dbConnect.php';
	if(isset($_POST["submit"]))
	{	
		//The form has been submitted and needs to be processed
		
		//Get the name value pairs from the $_POST variable into PHP variables
		//The example uses variables with the same name as the name atribute from the form
		$presenter_first_name = $_POST[presenter_first_name];
		$presenter_last_name = $_POST[presenter_last_name];
		$presenter_city = $_POST[presenter_city];
		$presenter_st = $_POST[presenter_st];
		$presenter_zip = $_POST[presenter_zip];
		$presenter_email = $_POST[presenter_email];
		$presenter_id = $_POST[presenter_id];	//from the hidden field of the update form
		
		//Create the SQL UPDATE query or command  
		$sql = "UPDATE presenters SET " ;
		$sql .= "presenter_first_name=?, ";
		$sql .= "presenter_last_name=?, ";
		$sql .= "presenter_city=?, ";
		$sql .= "presenter_st=?, ";
		$sql .= "presenter_zip=?, ";		
		$sql .= "presenter_email=? ";	//NOTE last one does NOT have a comma after it
		$sql .= " WHERE (presenter_id='$presenter_id')"; //VERY IMPORTANT  
		
		//echo "<h3>$sql</h3>";			//testing
	
		$query = $connection->prepare($sql);	//Prepare SQL query
	
		$query->bind_param('ssssss',$presenter_first_name,$presenter_last_name,		
			$presenter_city,$presenter_st,$presenter_zip,$presenter_email);
	
			if ( $query->execute() )
			{
				$message = "<h1>Your record has been successfully UPDATED the database.</h1>";
				$message .= "<p>Please <a href='presentersSelectView.php'>view</a> your records.</p>";
			}
			else
			{
				$message = "<h1>You have encountered a problem.</h1>";
				$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
			}
				
	}//end if submitted
	else	
	{
		//The form needs to display the fields of the record to the user for changes
		$updateRecId = $_GET['recId'];	//Record Id to be updated
		//$updateRecId = 2;				//Hard code a key for testing purposes
		
		echo "<h1>updateRecId: $updateRecId</h1>";
		
		//Finds a specific record in the table
		$sql = "SELECT presenter_id,presenter_first_name,presenter_last_name,presenter_city,presenter_st,presenter_zip,presenter_email FROM presenters WHERE presenter_id=?";	
		//echo "<p>The SQL Command: $sql </p>"; //For testing purposes as needed.
	
		$query = $connection->prepare($sql);
		
		$query->bind_param("i",$updateRecId);	
	
			if( $query->execute() )	//Run Query and Make sure the Query ran correctly
			{
				$query->bind_result($presenter_id,$presenter_first_name,$presenter_last_name,$presenter_city,$presenter_st,$presenter_zip,$presenter_email);
			
				$query->store_result();
				
				$query->fetch();
			}
			else
			{
				$message = "<h1>You have encountered a problem with your update.</h1>";
				$message .= "<h2>" . mysqli_error($connection) . "</h2>" ;			
			}
	
	}//end else submitted
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Presenters Admin Example</title>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h1>Presenters Admin System Example</h1>
<h3>UPDATE Form for Changing information on a Presenter</h3>
<p>This page is called from the presentersSelectView.php page when you click on the Update link of a presenter. That page attaches the presenter_id to the URL of this page making it a GET parameter.</p>
<p>This page uses that information to SELECT the requested record from the database. Then PHP is used to pull the various column values for the record and place them in the form fields as their default values. </p>
<p>The user/customer can make changes as needed or leave the information as is. When the form is submitted and validated it will update the record in the database.</p>
<p>Notice that this form uses a hidden field. The value of this hidden field contains the presenter_id. It is passed as one of the form name-value pairs. The submitted page will use that value to determine which record to update on the database.</p>

<?php
//If the user submitted the form the changes have been made
if(isset($_POST["submit"]))
{
	echo $message;	//contains a Success or Failure output content
}//end if submitted

else
{	//The page needs to display the form and associated data to the user for changes
?>
<form id="presentersForm" name="presentersForm" method="post" action="presentersUpdateForm.php">
  <p>Update the following Presenter's Information.  Place the new information in the appropriate field(s)</p>
  <p>First Name: 
    <input type="text" name="presenter_first_name" id="presenter_first_name" 
    	value="<?php echo $presenter_first_name; ?>"/>	<!-- PHP will put the name into the value of field-->
  </p>
  <p>Last Name:  
    <input type="text" name="presenter_last_name" id="presenter_last_name" 
    	value="<?php echo $presenter_last_name; ?>" />
  </p>
  <p>City:  
    <input type="text" name="presenter_city" id="presenter_city" 
       	value="<?php echo $presenter_city; ?>" />
  </p>
  <p>State: 
    <input type="text" name="presenter_st" id="presenter_st" 
        value="<?php echo $presenter_st; ?>" />
  </p>
  <p>Zip Code: 
    <input type="text" name="presenter_zip" id="presenter_zip" 
    	value="<?php echo $presenter_zip; ?>" />
  </p>
  <p>Email Address: 
    <input type="text" name="presenter_email" id="presenter_email" 
        value="<?php echo $presenter_email; ?>" />
  </p>
  
  	<!--The hidden form contains the record if for this record. 
    	You can use this hidden field to pass the value of record id 
        to the update page.  It will go as one of the name value
        pairs from the form.
    -->
  	<input type="hidden" name="presenter_id" id="presenter_id"
    	value="<?php echo $presenter_id ?>" />
  
  <p>
    <input type="submit" name="submit" id="submit" value="Update" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>

<?php
}//end else submitted
$query->close();
$connection->close();
?>
<p>Return to <a href="presentersLogin.php">Administrator Options</a></p>
</body>
</html>
