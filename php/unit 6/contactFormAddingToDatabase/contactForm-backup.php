            <?php include("email.php"); ?>

            <?php

            $nameErrMsg = "";
            $emailErrMsg = "";   
            $respErrMsg = "";
            $commentErrMsg = "";    

            $validForm = false;

            $inName = "";
            $inEmail = "";    
            $inResp = "";
            $inComment = "";
            $inNewsletterCheckbox = "";    
            $inMoreProductsCheckbox = "";
            $contact_newsletter = "";
            $contact_more_products = "";
            $contact_name = "";
            $contact_email = "";
            $contact_reason = "";
            $contact_comments = "";

            function validateName() {
              global $contact_name, $validForm, $nameErrMsg;    

              $nameErrMsg = "";                
              $contact_name = trim($contact_name);

                      if($contact_name=="")     
                      {
                        $validForm = false;         
                        $nameErrMsg = "Name Is Required"; 
                      }

                      if (!preg_match("/^[a-zA-Z ]*$/",$contact_name)) {
                        $validForm = false;
                        $nameErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

            function validateEmail()     
            {
              global $contact_email, $validForm, $emailErrMsg;    

              $contact_email = trim($contact_email);

                      if($contact_email=="")     
                      {
                        $validForm = false;         
                        $emailErrMsg = "Email Address Is Required"; 
                      }
                      if (!filter_var($contact_email, FILTER_VALIDATE_EMAIL)) {
                        $emailErrMsg = "Invalid email format"; 
                      }
                    }

            function validateResp()   
            {
              global $contact_reason, $validForm, $respErrMsg;  

              $respErrMsg = "";

                      if ($contact_reason =="")
                        {
                        $validForm = false;
                        $respErrMsg = "Reason For Contact Is Required";
                        } 
                    }

            function validateComment()      
            {
              global $contact_comments, $validForm, $commentErrMsg, $contact_reason;    

                      if ($contact_reason == "other" && $contact_comments == "")
                      {
                        $validForm = false;
                        $commentErrMsg = "Comments Required When Reason For Contact Is 'Other'";
                      }
                      if (!preg_match("/^[a-zA-Z ]*$/",$contact_comments)) {
                        $validForm = false;
                        $commentErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

if  (isset($_POST['submit']))
        {
          include 'dbConnect.php';
          $contact_name = $_POST['inName'];
          $contact_email = $_POST['inEmail'];
          $contact_reason = $_POST['inResp'];
          $contact_comments = $_POST['inComment'];
          $contact_newsletter = $_POST['inNewsletterCheckbox'];
          $contact_more_products = $_POST['inMoreProductsCheckbox'];

          $validForm = true;

          validateName();
          validateEmail();
          validateResp();
          validateComment();
        }?>

        <!DOCTYPE HTML>
        <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <title>PROGRAMMING PROJECT: Contact Form With PHP Validation</title>
          <style>
            .red  {
              color:red;
              font-style:italic;  
            }
          </style>
        </head>

        <body>
          <h1>WDV341 Intro PHP</h1>
          <h2>PROGRAMMING PROJECT: Contact Form With PHP Validation</h2>
          
            <?php

            if ($validForm)
              {
                   $newEmail = new myEmail($myEmail_to, $myEmail_subject, $myEmail_body, $myEmail_from);   

                   $newEmail->setFrom($contact_email); // ** this isnt working **
                   $newEmail->setTo("jmhall4@dmacc.edu");
                   $newEmail->setSubject("Customer Contact Form");
                   $newEmail->setBody("Reason for contact: " . " " . $inResp . " " . "\nCustomer added comments: " . " " . $inComment);

                   $newEmail->sendEmail();    

                  ?>

                  <h2>Thank you for contacting us, here are the details of your communication:</h2>
              
                  <?php 
                  echo "<h3>Your email address is: </h3>" . $newEmail->getFrom() . "<br>"; 
                  echo "<h3>Our email address is: </h3>" . $newEmail->getTo() . "<br>";
                  echo "<h3>Subject Line is: </h3>" . $newEmail->getSubject() . "<br>";
                  echo "<h3>Email Body is: </h3>" . $newEmail->getBody() . "<br>";
                  echo "<h3>Your email was sent at: </h3>" . date("l jS \of F Y h:i:s A");

                  //PUT CODE TO INSERT TO DB HERE

                  $sqlHardCode = "INSERT INTO wdv_341_customer_contacts (contact_name, contact_email, contact_reason, contact_comments, contact_newsletter, contact_more_products) VALUES (?,?,?,?,?,?);";

                  $stmt = $link->prepare($sqlHardCode); 

                  $stmt->bind_param("ssssss",$contact_name,$contact_email,$contact_reason,$contact_comments,$contact_newsletter,$contact_more_products);

                                      if  ( $stmt->execute()){
                        //echo '<script type="text/javascript">alert("execute");</script>';

                        $message = "<h1>YIPPEE! Your record has been successfully added to the database.</h1>";
                        //$message .= "<p>Please <a href='eventsSelectView.php'>view</a> your records.</p>";
                        }
                  else
                        {
                        $message = "<h1>You have encountered a big big problem.</h1>";
                        $message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>"; //remove this for production purposes
                        }
            
                    $stmt->close();
                    $link->close(); 

                  ?>

                  <?php
              } //end the true branch of the form view area



else

      { 
        ?>

      <header>Contact Form</header>
      <form name="form1" class="topBefore" method="post" action="contactForm.php">
        <p>&nbsp;</p>
        <p>
          <label>Your Name:
            <input type="text" name="inName" id="inName" value="<?php echo $contact_name;?>"><p class="red"><?php echo "$nameErrMsg";?></p>
          </label>
        </p>
        <p>Your Email: 
          <input type="text" name="inEmail" id="inEmail" value="<?php echo $contact_email;?>"/><p class="red"><?php echo "$emailErrMsg";?></p>
        </p>
        <p>Reason for contact: <p class="red"><?php echo "$respErrMsg";?></p>
          <label>
            <select name="inResp" id="inResp">
              <option value="">Please Select a Reason</option>
              <option value="problem"<?php if($contact_reason == 'problem'){echo("selected");}?>>Product Problem</option>
              <option value="return"<?php if($contact_reason == 'return'){echo("selected");}?>>Return a Product</option>
              <option value="other"<?php if($contact_reason == 'other'){echo("selected");}?>>Other</option>
            </select>
          </label>
        </p>
        <p>
          <label>Comments:
            <textarea name="inComment" id="inComment" cols="45" rows="5"/><?php echo $contact_comments;?></textarea>
          </label>
        </p>
        <p class="red"><?php echo "$commentErrMsg";?></p>
        </p>
        <p>
        <label><input type="checkbox" name="inNewsletterCheckbox" id="inNewsletterCheckbox" value="yes"> Yes, I would like to receive your newsletter.</label><br>
        <label><input type="checkbox" name="inMoreProductsCheckbox" id="inMoreProductsCheckbox" value="yes"> Yes, I would like to receive additional information regarding related products.</label><br>

     
        
        contact_date:
        contact_time:

        <p>
          <input type="submit" name="submit" id="submit" value="Submit">
          <input type="reset" name="reset" id="reset" value="Reset">
        </p>
      </form>

      <?php
      } //end else branch for the View area
      ?>

</body>
</html>
