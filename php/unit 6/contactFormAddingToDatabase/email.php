<?php

	class myEmail	{	
		  var $from;
          var $to;
          var $subject;
          var $body;

			// CONSTRUCTOR

          	function __construct($myEmail_from, $myEmail_to, $myEmail_subject, $myEmail_body) 
          	{
          		$this->from = $myEmail_from;
          		$this->to = $myEmail_to;
          		$this->subject = $myEmail_subject;
          		$this->body = $myEmail_body;
          	}

          	//SETTERS START HERE

 			function setFrom($inFrom) 
				{
				$this->from = $inFrom;
				}			
			function setTo($inTo) 
				{
				$this->to = $inTo;
				}	
			function setSubject($inSubject) 
				{
				$this->subject = $inSubject;
				}	
			function setBody($inBody) 
				{
				$this->body = $inBody;
				}	

			// GETTERS START HERE
			
			function getFrom() 
				{
				return $this->from;
				}			
			function getTo() 
				{
				return $this->to;
				}	
			function getSubject() 
				{
				return $this->subject;
				}	
			function getBody() 
				{
				return $this->body;
				}	

			// ADDITIONAL METHODS START HERE

			function sendEmail()	
				{
				mail($this->to, $this->subject, $this->body, $this->from);
				//mail($this->subject, $this->body, $this->from, $this->to);
				}	
				
	}	//end Product class
?>