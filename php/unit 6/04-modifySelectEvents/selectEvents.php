<?php 
session_cache_limiter('none');  //This prevents a Chrome error...
session_start();
 
	if ($_SESSION['validUser'] == "yes")		//is valid user?
	{

//The following section of PHP acts as the Controller.  It contains the processing logic
//needed to gather the data from the database table.  Format the data into a presentation
//format that can be viewed on the client's browser.


	include 'dbConnect.php';				//connects to the database

	$event_id = "";
	
	$sql = "SELECT * FROM wdv341_event";		//build the SQL command	

	$res = $link->query($sql);	//run the query
	
	if($res)
	{
		//process the result
		if ($res->num_rows > 0) 
		{
			$displayMsg = "<h1>Number of rows is " . $res->num_rows . "</h1>";			
			// output data of each row
			
			$displayMsg .= "<table id='jerTable'><tr id='heading'><td>NAME</td><td>DESCRIPTION</td><td>PRESENTER</td><td>DATE</td><td>TIME</td></tr>";
			while($row = $res->fetch_assoc()) 
			{
				$displayMsg .= "<td>";
				$displayMsg .= $row['event_name'].'</td><td>'.$row['event_description'].'</td><td>'.$row['event_presenter'].'</td><td>'.$row['event_date'].'</td><td>'.$row['event_time'];
				$displayMsg .= "</td><td><a href=\"delete.php?event_id=".$row['event_id']."\">Delete</a></td>";
				$displayMsg .= "</tr>";
			}
			$displayMsg .= "</table>";
		} 
		else 
		{
			$displayMsg .= "0 results";
		}		
	}
	else
	{
		//display error message for DEVELOPMENT purposes
		$displayMsg .= "<h3>Sorry there has been a problem</h3>";
		$displayMsg .= "<p>" . mysqli_error($link) . "</p>";			//Display error message
	}
	$link->close();
	
	
//The following HTML or markup is the VIEW.  This will be sent to the client for display in their browser.
//Notice that we echo the $displayMsg variable which contains the formatted output that we created in the 
//Controller area above.  	
?>
<html>
<head>
	<title>WDV341 SELECT Example</title>
	<link rel="stylesheet" type="text/css" href="mystyle.css">

</head>
<body>
	<h1>We found the following information.</h1>
	<div id="content">
		<?php echo $displayMsg; ?>
	</div>

<?php
	}

	else
	{
	?>
	<h1>YOU NEED TO <a href="http://www.jeremymhall.info/files/login/login.php">LOGIN</a></h1>
	<?php
	}
	?>

</body>
</html>

