<?php

//The following section of PHP acts as the Controller.  It contains the processing logic
//needed to gather the data from the database table.  Format the data into a presentation
//format that can be viewed on the client's browser.


	include 'dbConnectOOP.php';				//connects to the database
	
	$sql = "SELECT event_name,event_description,event_presenter,event_date,event_time FROM wdv341_event WHERE event_presenter='Travis'";		//build the SQL command	

	$res = $link->query($sql);	//run the query
	
	if($res)
	{
		//process the result
		if ($res->num_rows > 0) 
		{
			$displayMsg = "<h1>Number of rows is " . $res->num_rows . "</h1>";			
			// output data of each row
			
			$displayMsg .= "<table id='jerTable'><tr id='heading'><td>NAME</td><td>DESCRIPTION</td><td>PRESENTER</td><td>DATE</td><td>TIME</td></tr>";
			while($row = $res->fetch_assoc()) 
			{
				$displayMsg .= "<td>";
				$displayMsg .= $row['event_name'].'</td><td>'.$row['event_description'].'</td><td>'.$row['event_presenter'].'</td><td>'.$row['event_date'].'</td><td>'.$row['event_time'];
				$displayMsg .= "</td><td>";
				$displayMsg .= "</td></tr>";
			}
			$displayMsg .= "</table>";
		} 
		else 
		{
			$displayMsg .= "0 results";
		}		
	}
	else
	{
		//display error message for DEVELOPMENT purposes
		$displayMsg .= "<h3>Sorry there has been a problem</h3>";
		$displayMsg .= "<p>" . mysqli_error($link) . "</p>";			//Display error message
	}
	$link->close();
	
	
//The following HTML or markup is the VIEW.  This will be sent to the client for display in their browser.
//Notice that we echo the $displayMsg variable which contains the formatted output that we created in the 
//Controller area above.  	
?>
<html>
<head>
	<title>WDV341 SELECT Example</title>
	<link rel="stylesheet" type="text/css" href="mystyle.css">

</head>
<body>
	<h1>We found the following information.</h1>
	<div id="content">
		<?php echo $displayMsg; ?>
	</div>

</body>
</html>

