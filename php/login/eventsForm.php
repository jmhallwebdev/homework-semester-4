<?php 
session_cache_limiter('none');  //This prevents a Chrome error...
session_start();
 
  if ($_SESSION['validUser'] == "yes")    //is valid user?
  {
        	$nameErrMsg = "";
        	$descErrMsg = "";
        	$presErrMsg = "";
          $dateErrMsg = "";
          $timeErrMsg = "";
        	$validForm = false;
        	$event_name = "";
        	$event_description = "";
        	$event_presenter = "";
          $event_date = "";
          $event_time = "";
          $message = "";

          function validateName() {
                  global $event_name, $validForm, $nameErrMsg; 
        
                  $nameErrMsg = "";   
                  $event_name = trim($event_name);
                          if($event_name == "")     
                          {
                            $validForm = false;     
                            $nameErrMsg = "Name Is Required"; 
                          }
                          if (!preg_match("/^[a-zA-Z0-9 ]*$/",$event_name)) {
                            $validForm = false;
                            $nameErrMsg = "Only letters, numbers, & white space allowed"; 
                          } 
                        }

          function validateDesc() {
                  global $event_description, $validForm, $descErrMsg;    

                  $descErrMsg = "";                
                  $event_description = trim($event_description);
                          if($event_description=="")     
                          {
                            $validForm = false;         
                            $descErrMsg = "Description Is Required"; 
                          }
                          if (!preg_match("/^[a-zA-Z0-9 ]*$/",$event_description)) {
                            $validForm = false;
                            $descErrMsg = "Only letters, numbers, & white space allowed"; 
                          } 
                        }

          function validatePres() {
                  global $event_presenter, $validForm, $presErrMsg;    

                  $presErrMsg = "";                
                  $event_presenter = trim($event_presenter);
                        if (!preg_match("/^[a-zA-Z ]*$/",$event_presenter)) {
                            $validForm = false;
                            $presErrMsg = "Only letters & white space allowed"; 
                          } 
                        if($event_presenter=="")     
                          {
                            $validForm = false;         
                            $presErrMsg = "Presenter Is Required"; 
                          }
                        }

          function validateDate() {
                  global $event_date, $validForm, $dateErrMsg; 
        
                  $dateErrMsg = "";   

                          if($event_date == "")     
                          {
                            $validForm = false;     
                            $dateErrMsg = "Event Date Is Required"; 
                          }
                        }

          function validateTime() {
                  global $event_time, $validForm, $timeErrMsg; 
        
                  $timeErrMsg = "";   

                          if($event_time == "")     
                          {
                            $validForm = false;     
                            $timeErrMsg = "Event Time Is Required"; 
                          }
                        }


if  (isset($_POST["submit"]))
    {	
  		include 'dbConnect.php';
  		$event_name = $_POST['event_name'];
  		$event_description = $_POST['event_description'];
  		$event_presenter = $_POST['event_presenter'];
  		$event_date = $_POST['event_datepicker'];
  		$event_time = $_POST['event_timepicker'];

  		$validForm = true;

      validateName();
      validateDesc();
      validatePres();
      validateDate();
      validateTime();
      }
    ?>

<?php
            if ($validForm)
              {
                
                $sqlHardCode = "INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time) VALUES (?,?,?,?,?);";

                //echo($sqlHardCode);

                // prepare and bind
                $stmt = $link->prepare($sqlHardCode); //Prepares the query statement 
                        
                //Binds the parameters to the query.  
                //The sssii are the data types of the variables in order. 

                $stmt->bind_param("sssss",$event_name,$event_description,$event_presenter,$event_date,$event_time);

                //Run the SQL prepared statements

                    if  ( $stmt->execute()){
                        //echo '<script type="text/javascript">alert("execute");</script>';

                        $message = "<h1>Hooray! Your record has been successfully added to the database.</h1>";
                        //$message .= "<p>Please <a href='eventsSelectView.php'>view</a> your records.</p>";
                        }
                  else
                        {
                        $message = "<h1>You have encountered a problem.</h1>";
                        $message .= "<h2 style='color:red'>" . mysqli_error($connection) . "</h2>"; //remove this for production purposes
                        }
            
                    $stmt->close();
                    $link->close(); 

              } //end the true branch of the form view area
            ?>  

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP  - Event Admin Example</title>
<style>
          .red  {
            color:red;
            font-style:italic;  
          }
        </style>
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<link href="jquery-ui-timepicker-addon.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="jquery-ui-timepicker-addon.js"></script>
<script>
  $(function() {
            $("#event_datepicker").datepicker({
               dateFormat:"yy-mm-dd",
            });
         });
  </script>
  <script>
  $(function() {
            $('#event_timepicker').timepicker();
         });

  </script>
  <script type="text/javascript">
  function validateMyForm() {
    // The field is empty, submit the form.
    if(!document.getElementById("honeypot").value) { 
      return true;
    } 
     // the field has a value it's a spam bot
    else {
      return false;
    }
  }
</script>
</head>
<body>
  <h1>WDV341 Event Admin System Example</h1>
  <h3>Input Form for Adding Events</h3>



  <?php
  if($validForm)
    { 
     
     //If the form was submitted display the INSERT result message
    ?>
     <h3><?php echo($message); ?></h3>
    <?php
    }//end if
    else{
    ?>

    <p>This is the input form that allows the user/customer to enter the information for an Event. Once the form is submitted and validated it will call the eventsForm.php page. That page will pull the form data into the PHP and add a new record to the database.</p>


    <header>Event Form</header>
    <form id="eventsForm" name="eventsForm" method="post" action="eventsForm.php" onsubmit="return validateMyForm();">
    <p>Add a new Event</p>
    <p>Event Name: 
    <input type="text" name="event_name" id="event_name" value="<?php echo $event_name;?>"></p><p class="red"><?php echo "$nameErrMsg";?></p>
    <p>Event Description:  
    <input type="text" name="event_description" id="event_description" value="<?php echo $event_description;?>"></p><p class="red"><?php echo "$descErrMsg";?></p>
    <p>Event Presenter: 
    <input type="text" name="event_presenter" id="event_presenter" value="<?php echo $event_presenter;?>"></p><p class="red"><?php echo "$presErrMsg";?></p>
    <p>Date: <input type="text" value="<?php echo $event_date;?>" name = "event_datepicker" id="event_datepicker"></p>
    <p class="red"><?php echo "$dateErrMsg";?></p>
    <p>Time: <input type="text" value="<?php echo $event_time;?>" name = "event_timepicker" id="event_timepicker"></p>
    <p class="red"><?php echo "$timeErrMsg";?></p>

    <!--Honeypot is below -->
    <div style="display:none;">
    <label>Keep this field blank</label>
    <input type="text" name="honeypot" id="honeypot" />
    </div>
    <!--Honeypot is above -->


    <p>
    <input type="submit" name="submit" id="submit" value="Add Event" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
    </p>
    </form>

    <p>RETURN TO <a href='http://www.jeremymhall.info/files/login/login.php'>LOGIN</a></p>

<?php

    }

    ?>

    <?php
    }

  else
  { 
    ?>
    <h1>YOU NEED TO <a href="http://www.jeremymhall.info/files/login/login.php">LOGIN</a></h1>
  <?php 
  }
    ?>

</body>
</html>
