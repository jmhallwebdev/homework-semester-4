<?php 

	//CONTROLLER SECTION

	$yourName = "jeremy";
	$h1 = "<h1>PHP Basics</h1>";
	$number1 = "10";
	$number2 = "20";
	$total = "$number1" + "$number2";

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>My PHP Document</title>
</head>
<body>
	
	<?php echo $h1; ?>	
	<h2>Hello my name is <?php echo $yourName; ?></h2>
	<h3>Variable 1 is <?php echo $number1; ?></h3>
	<h3>Variable 2 is <?php echo $number2; ?></h3>
	<h3>The total of the 2 variables is <?php echo $total; ?></h3>

</body>
</html>