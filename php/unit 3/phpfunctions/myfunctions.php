<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>My Functions Page</title>
	<?php
	//$myNumber = 1234567890; // sets global scope variables
	//$myCurrency = 123456; 
	// 1. A function that accepts a date and formats it into m/d/Y
	function formatDateMdY($inDate)
	{
		$date=date_create($inDate);
		echo date_format($date,"m/d/Y");
	}
	// 2. A function that accepts a date and formats it into the intl format of d/m/Y
	function formatDateDmY($inDate)
	{
		$date=date_create($inDate);
		echo date_format($date,"d/m/Y");
	}
	function checkMyString($inString) //$inString is the string input
	{
		$newString=$inString.trim(); //trims (b)
		$length = strlen($newString);  //outputs the length (a)
		echo "the string length is  " . $length . "<br>";
		$lower = strtolower($newString); //outputs the lowercase version (c)
		echo "the string in lowercase characters is:  " . $lower . "<br>";
		$containDMACC =  stristr($newString,"dmacc"); 
		//outputs if dmacc or DMACC is in the input string 
		if ($containDMACC=false){    
			echo "does the string contain DMACC? = no ";
		}
		else {
			echo "does the string contain DMACC? = yes";
		}}
		function formatMyNumber($inNumber) //$inNumber is the input number to format
		{
			echo number_format($inNumber)."<br>";
		}
		function formatCurrencyNumber($inNumber) //$inNumber is the input number to format
		{
			$formattedNum = number_format($inNumber, 2);
			echo "&#36;" . $formattedNum;
		}
?>
</head>
<body>
	<h1>My Functions Page</h1>
	<!-- Here is where we setup what will be seen on the page and where the echoes go -->
	<p><img src="assets/images/pic.png"></p>
	<p><img src="assets/images/pic2.png"></p>
	<p><img src="assets/images/pic3.png"></p>
	<p>
	<h3>// 1. A function that accepts a date and formats it into m/d/Y - since this needs a date object I fed it "2016-09-16"</h3>
	<?php
	formatDateMdY("2016-09-16");
	?></p>

	<p>
	<h3>// 2. A function that accepts a date and formats it into the intl format of d/m/Y - since this needs a date object I fed it "2016-09-16"</h3>
	<?php
	formatDateDmY("2016-09-16");
	?></p>	
	<p>
	<h3>// 3. Create a function that will accept a string input.  It will do the following things to the string: A. Display the number of characters in the string B. Trim any leading or trailing whitespace C. Display the string as all lowercase characters and D. Will display whether or not the string contains "DMACC" either upper or lowercase - I fed it "   qwertydmacclkjhg   "</h3>
 	<?php
	 checkMyString("   QWERTYdmacclkjhg   ");
	?></p>
	<p>
	<h3>// 4. Create a function that will accept a number and display it as a formatted number.   Use 1234567890 for your testing.</h3>
	</p>
	<?php
	 formatMyNumber(1234567890);
	?>
	<p>
	<h3>// 5. Create a function that will accept a number and display it as US currency.  Use 123456 for your testing.</h3>
	<?php
	 formatCurrencyNumber(123456);
	?>
</body>
</html>